use std::io::Write;
use std::io::BufReader;
use std::io::Read;
use std::path::PathBuf;
//use path_absolutize::*;

pub struct VersionData {
    pub rustc: PathBuf,
    pub cargo: PathBuf,
    pub url: String,
    pub config: String,
    pub output_file: PathBuf,
    pub output_dir: PathBuf,
    pub git_command: String,
}

impl VersionData {
    pub fn new(version: impl Into<String>, rustc_versions: Vec<impl Into<String>>, base_directory: impl Into<PathBuf>, target_triple: impl Into<String>) -> Self {
        let ver = version.into();
        let base_dir = base_directory.into();
        let mut versions: Vec<String> = Vec::new();
        let mut cargo_init = base_dir.clone();
        cargo_init.push("/usr/bin/cargo");
        let mut cargo = cargo_init.clone();
        let triple = target_triple.into();
        let mut rustc_init = base_dir.clone();
        rustc_init.push("/usr/bin/rustc");
        let mut rustc = rustc_init.clone();
        let url = format!("https://static.rust-lang.org/dist/rustc-{}-src.tar.gz", ver);
        let git_command = format!("git clone https://github.com/smaeul/rust.git -b bootstrap-{} --depth 1 bootstrap-{}", ver, ver);
        let mut output_file = base_dir.clone();
        output_file.push(format!("rustc-{}-src.tar.gz", ver).as_str());
        let mut output_dir = base_dir.clone();
        output_dir.push(format!("bootstrap-{}", ver).as_str());
        for vers in rustc_versions {
            let vrsn = vers.into();
            versions.push(vrsn);
        }
        for (num, vers) in versions.clone().into_iter().enumerate() {
            let rustc_version: String = vers.into();
            if rustc_version == ver {
                if num != 0 {
                    let mut cargo_start = base_dir.clone();
                    cargo_start.push(format!("bootstrap-{}/build/{}/stage2-tools-bin/cargo", versions.clone()[num - 1], triple));
                    cargo = cargo_start.clone();//PathBuf::new(format!().as_str().absolutize.unwrap().into_owned())
                    let mut rustc_start = base_dir.clone();
                    rustc_start.push(format!("bootstrap-{}/build/{}/stage2/bin/rustc", versions.clone()[num - 1], triple).as_str());
                    rustc = rustc_start.clone();
                }
            }
        }
        let config = format!(r#"[build]
cargo = "{}"
rustc = "{}"
docs = false
vendor = true
extended = true
tools = ["cargo"]
build = "{}"
[rust]
channel = "stable"
#[target.{}]
#crt-static = false"#, cargo.clone().to_str().unwrap(), rustc.clone().to_str().unwrap(), triple, triple);
        Self {
            rustc: rustc,
            cargo: cargo,
            url: url,
            config: config,
            output_file: output_file,
            output_dir: output_dir,
            git_command: git_command,
        }
    }
}

pub fn buffered_download(url: impl Into<String>, output: impl Into<PathBuf>) -> Result<(), anyhow::Error> {
    let url_string = url.into();
    let out_path = output.into();
    let mut outfile = std::fs::File::create(out_path)?;
    let response = ureq::get(url_string.as_str()).call()?;
    let mut reader = BufReader::new(response.into_reader());
    let mut keep_going = true;
    while keep_going {
        let buf = read_bytes(&mut reader)?;
        if !buf.is_empty() {
            outfile.write_all(&buf)?;
        } else {
            keep_going = false;
        }
    }
    Ok(())

}

fn read_bytes(reader: &mut (impl Read + Send)) -> Result<Vec<u8>, anyhow::Error> {
    let mut chunk = reader.take(409600000);
    let mut buf: Vec<u8> = Vec::new();
    chunk.read_to_end(&mut buf)?;
    Ok(buf)
}
